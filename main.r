library(tidyverse)
library(SharpeR)
library(PortfolioAnalytics)
library(car)

source("utils.r")
source("strategies.r")
#set.seed(5000)

### load data
returns <- read_csv("data/currency_returns.csv")
returns <- xts(returns[,-1],returns$date)["2006-06-01/2020-04-01"]

risk_free <- read_csv("data/risk_free.csv")
risk_free <- xts(risk_free[,-1],  risk_free$date)

excess_returns <- Return.excess(returns, risk_free) #verify: returns[,1] - excess_returns[,1] == risk_free


returns_sp500 <- read_csv("data/sp500_returns.csv")
returns_sp500 <- xts(returns_sp500[,-1],  returns_sp500$date)

## Calculate metrics
one_over_n_weights <- weights_for_all_periods(excess_returns, one_over_n, 50)
one_over_n_portfolio_returns <- portfolio_return_timeseries(one_over_n_weights, returns)
over_n_sharp <- mean(one_over_n_portfolio_returns-risk_free[(length(risk_free)-length(one_over_n_portfolio_returns)+1):length(risk_free)])/sd(one_over_n_portfolio_returns)

min_variance_weights <- weights_for_all_periods(excess_returns, min_var, 50)
min_var_portfolio_returns <- portfolio_return_timeseries(min_variance_weights, returns)
min_var_sharp <- mean(min_var_portfolio_returns-risk_free[(length(risk_free)-length(min_var_portfolio_returns)+1):length(risk_free)])/sd(min_var_portfolio_returns)

## gamma = 1

markowitz_weights_1 <- weights_for_all_periods(excess_returns, markowitz, 50, gamma = 1)
markowitz_portfolio_returns_1 <- portfolio_return_timeseries(markowitz_weights_1, returns)
mark_sharp_1 <- mean(markowitz_portfolio_returns_1-risk_free[(length(risk_free)-length(markowitz_portfolio_returns_1)+1):length(risk_free)])/sd(markowitz_portfolio_returns_1)

markowitz_longonly_weights_1 <- weights_for_all_periods(excess_returns, markowitz_longonly, 50, gamma = 1)
markowitz_longonly_portfolio_returns_1 <- portfolio_return_timeseries(markowitz_longonly_weights_1, returns)
mark_longonly_sharp_1 <- mean(markowitz_longonly_portfolio_returns_1-risk_free[(length(risk_free)-length(markowitz_longonly_portfolio_returns_1)+1):length(risk_free)])/sd(markowitz_longonly_portfolio_returns_1)  

bayes_weights_1 <- weights_for_all_periods(excess_returns, bayes_stein, 50, gamma = 1)
bayes_portfolio_returns_1 <- portfolio_return_timeseries(bayes_weights_1, returns)
bayes_sharp_1 <- mean(bayes_portfolio_returns_1-risk_free[(length(risk_free)-length(bayes_portfolio_returns_1)+1):length(risk_free)])/sd(bayes_portfolio_returns_1)

bayes_longonly_weights_1 <- weights_for_all_periods(excess_returns, bayes_stein_longonly, 50, gamma = 1)
bayes_longonly_portfolio_returns_1 <- portfolio_return_timeseries(bayes_longonly_weights_1, returns)
bayes_longonly_sharp_1 <- mean(bayes_longonly_portfolio_returns_1-risk_free[(length(risk_free)-length(bayes_longonly_portfolio_returns_1)+1):length(risk_free)])/sd(bayes_longonly_portfolio_returns_1)

## gamma = 2

markowitz_weights_2 <- weights_for_all_periods(excess_returns, markowitz, 50, gamma = 2)
markowitz_portfolio_returns_2 <- portfolio_return_timeseries(markowitz_weights_2, returns)
mark_sharp_2 <- mean(markowitz_portfolio_returns_2-risk_free[(length(risk_free)-length(markowitz_portfolio_returns_2)+1):length(risk_free)])/sd(markowitz_portfolio_returns_2)

markowitz_longonly_weights <- weights_for_all_periods(excess_returns, markowitz_longonly, 50, gamma = 2)
markowitz_longonly_portfolio_returns_2 <- portfolio_return_timeseries(markowitz_longonly_weights, returns)
mark_longonly_sharp_2 <- mean(markowitz_longonly_portfolio_returns_2-risk_free[(length(risk_free)-length(markowitz_longonly_portfolio_returns_2)+1):length(risk_free)])/sd(markowitz_longonly_portfolio_returns_2)  

bayes_weights <- weights_for_all_periods(excess_returns, bayes_stein, 50, gamma = 2)
bayes_portfolio_returns_2 <- portfolio_return_timeseries(bayes_weights, returns)
bayes_sharp_2 <- mean(bayes_portfolio_returns_2-risk_free[(length(risk_free)-length(bayes_portfolio_returns_2)+1):length(risk_free)])/sd(bayes_portfolio_returns_2)

bayes_longonly_weights <- weights_for_all_periods(excess_returns, bayes_stein_longonly, 50, gamma = 2)
bayes_longonly_portfolio_returns_2 <- portfolio_return_timeseries(bayes_longonly_weights, returns)
bayes_longonly_sharp_2 <- mean(bayes_longonly_portfolio_returns_2-risk_free[(length(risk_free)-length(bayes_longonly_portfolio_returns_2)+1):length(risk_free)])/sd(bayes_longonly_portfolio_returns_2)

bayes_longonly_sharp_1
bayes_longonly_sharp_2
mark_longonly_sharp_1
mark_longonly_sharp_2
mark_sharp_1
mark_sharp_2
min_var_sharp
bayes_sharp_1
bayes_sharp_2
over_n_sharp

# test sharpe ratios frage: excess oder nicht excess

test_min <- cbind(min_var_portfolio_returns, one_over_n_portfolio_returns)
sr_equality_test(test_min)

test_mark_1 <- cbind(markowitz_portfolio_returns_1, one_over_n_portfolio_returns)
sr_equality_test(test_mark_1)

test_mark_2 <- cbind(markowitz_portfolio_returns_2, one_over_n_portfolio_returns)
sr_equality_test(test_mark_2)

test_mark_long_1 <- cbind(markowitz_longonly_portfolio_returns_1, one_over_n_portfolio_returns)
sr_equality_test(test_mark_long_1)

test_mark_long_2 <- cbind(markowitz_longonly_portfolio_returns_2, one_over_n_portfolio_returns)
sr_equality_test(test_mark_long_2)

test_bayes_1 <- cbind(bayes_portfolio_returns_1, one_over_n_portfolio_returns)
sr_equality_test(test_bayes_1)

test_bayes_2 <- cbind(bayes_portfolio_returns_2, one_over_n_portfolio_returns)
sr_equality_test(test_bayes_2)

test_bayes_long_1 <- cbind(bayes_longonly_portfolio_returns_1, one_over_n_portfolio_returns)
sr_equality_test(test_bayes_long_1)

test_bayes_long_2 <- cbind(bayes_longonly_portfolio_returns_2, one_over_n_portfolio_returns)
sr_equality_test(test_bayes_long_2)

# CAPM alpha

returns_sp500 <- as.vector(returns_sp500$`^GSPC`)

lm_one <- lm(one_over_n_portfolio_returns ~ returns_sp500)
lm_min_var <- lm(min_var_portfolio_returns ~ returns_sp500)
lm_mark_1 <- lm(markowitz_portfolio_returns_1 ~ returns_sp500)
lm_mark_2 <- lm(markowitz_portfolio_returns_2 ~ returns_sp500)
lm_mark_long_1 <- lm(markowitz_longonly_portfolio_returns_1 ~ returns_sp500)
lm_mark_long_2 <- lm(markowitz_longonly_portfolio_returns_2 ~ returns_sp500)
lm_bayes_1 <- lm(bayes_portfolio_returns_1 ~ returns_sp500)
lm_bayes_2 <- lm(bayes_portfolio_returns_2 ~ returns_sp500)
lm_bayes_long_1 <- lm(bayes_longonly_portfolio_returns_1 ~ returns_sp500)
lm_bayes_long_2 <- lm(bayes_longonly_portfolio_returns_2 ~ returns_sp500)

#test alpha

linearHypothesis(model = lm_min_var, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")
linearHypothesis(model = lm_mark_1, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")
linearHypothesis(model = lm_mark_2, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")
linearHypothesis(model = lm_mark_long_1, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")
linearHypothesis(model = lm_mark_long_2, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")
linearHypothesis(model = lm_bayes_1, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")
linearHypothesis(model = lm_bayes_2, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")
linearHypothesis(model = lm_bayes_long_1, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")
linearHypothesis(model = lm_bayes_long_2, hypothesis.matrix = c(1,0), rhs = lm_one$coefficients[1], white.adjust = "hc0")


