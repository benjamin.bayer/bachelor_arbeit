library(tidyverse)
library(tidyquant)

symbols <- c("AAPL", "ADDYY", "AMZN", "AXP", "CMG", "CVS", "DE", "DIS",
             "GAZ.DE", "IBM", "ISRG", "MA", "NFLX", "NKE", "NVO", "SBUX",
             "SHW", "UAA")

assets_raw <- symbols %>% 
  tq_get(get = "stock.prices",
         from = "2006-06-01",
         periodicity = "monthly")


assets_return <- assets_raw %>%  group_by(symbol) %>% 
  tq_transmute(select = adjusted,
               mutate_fun = periodReturn,
               type = "log") %>% 
  spread(symbol, monthly.returns) %>% 
  slice(c(-1)) %>% head(-1) %>% as.data.frame()


write_csv(assets_return, "data/returns.csv")


#### Risk free

getSymbols(Symbols = "GS3M", src = "FRED", auto.assign = FALSE) -> risk_free_raw ##https://fred.stlouisfed.org/series/GS3 Citation: Board of Governors of the Federal Reserve System (US), 3-Month Treasury Constant Maturity Rate [GS3M], retrieved from FRED, Federal Reserve Bank of St. Louis; https://fred.stlouisfed.org/series/GS3M, April 3, 2020.

risk_free <- risk_free_raw %>% transform("GS3M" = (GS3M/100 + 1)^(1/12) - 1) %>% window(start = "2006-06-01") %>% as.data.frame()
risk_free$date <- risk_free_raw %>% window(start = "2006-06-01") %>% time()

risk_free <- risk_free %>% select(date, GS3M)
write_csv(risk_free, "data/risk_free.csv")


#### currencies from yahoo finance

currency_symbols <- c("EURUSD=X", "JPYUSD=X", "GBPUSD=X", "AUDUSD=X", "NZDUSD=X", "CADUSD=X", "SEKUSD=X", "CHFUSD=X", "HUFUSD=X", "CNYUSD=X", 
                      "HKDUSD=X", "SGDUSD=X", "INRUSD=X", "MXNUSD=X", "PHPUSD=X", "THBUSD=X", "MYRUSD=X", "ZARUSD=X", "RUBUSD=X")

#currencies <- currency_symbols %>% 
#  tq_get(get = "stock.prices",
#         from = "1980-01-01",
#          periodicity = "monthly")


#currencies %>% group_by(symbol) %>% summarise(mindate = min(date)) %>% summarise(max(mindate)) 

currencies <- currency_symbols %>% 
  tq_get(get = "stock.prices",
         from = "2006-05-01",
         periodicity = "monthly")


currency_returns <- currencies %>%  group_by(symbol) %>% 
  tq_transmute(select = adjusted,
               mutate_fun = periodReturn,
               type = "log") %>% 
  spread(symbol, monthly.returns) %>% 
  head(-1) %>% tail(-1) %>% as.data.frame() 

write_csv(currency_returns, "data/currency_returns.csv")



