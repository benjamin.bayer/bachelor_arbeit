##only max sharpt ratio

markowitz_alt <-function(return_matrix, gamma) {
  M <- nrow(return_matrix)
  N <- ncol(return_matrix)
  mean_returns <- t(rep(1/M, M)) %*% return_matrix
  cov_matrix <- cov(return_matrix)
  weights <- as.vector(solve(cov_matrix) %*% t(mean_returns)) / (rep(1, N) %*% solve(cov_matrix) %*% t(mean_returns))
  return(weights)
}


markowitz_alt_quad <- function(return_matrix, gamma){
  N     <- ncol(return_matrix)     # number of observations
  M     <- nrow(return_matrix)      # number of assets
  mData  <- array(return_matrix,dim = c(M, N))
  # riskfree rate (2.5% pa)
  mean_returns     <- apply(mData,2, mean)    # means
  # excess means
  
  # qp
  aMat  <- as.matrix(mean_returns)
  bVec  <- 1
  zeros <- array(0, dim = c(N,1))
  solQP <- solve.QP(cov(mData), zeros, aMat, bVec, meq = 1)
  
  # rescale variables to obtain weights
  weights <- as.vector(solQP$solution/sum(solQP$solution))
  
  # compute sharpe ratio
  return(weights)
}


markowitz_alt_quad_longonly <- function(return_matrix, gamma){ ##problem when all means are negative
  N     <- ncol(return_matrix)     # number of observations
  M     <- nrow(return_matrix)      # number of assets
  mData  <- array(return_matrix,dim = c(M, N))
  # riskfree rate (2.5% pa)
  mean_returns     <- apply(mData,2, mean)    # means
  # excess means
  
  # qp
  print(c(mean_returns, N))
  aMat  <- matrix( c(as.matrix(mean_returns), diag(nrow=N)), nrow=N)
  bVec  <- c(1, rep(0,N))
  zeros <- array(0, dim = c(N,1))
  solQP <- solve.QP(cov(mData), zeros, aMat, bVec, meq = 1)
  
  # rescale variables to obtain weights
  weights <- as.vector(solQP$solution/sum(solQP$solution))
  
  # compute sharpe ratio
  return(weights)
}


markowitz <-function(return_matrix, gamma) {
  M <- nrow(return_matrix)
  N <- ncol(return_matrix)
  mean_returns <- t(t(rep(1/M, M)) %*% return_matrix)
  sigma <- cov(return_matrix)
  invers_simga <- solve(sigma)
  einheits <- as.matrix(rep(1, N))
  brack_1 <- as.numeric((t(einheits) %*% invers_simga %*% mean_returns - gamma)/(t(einheits) %*% invers_simga %*% einheits ))
  brack_2 <- mean_returns - brack_1 * einheits
  weights <- as.vector((1/gamma) * invers_simga %*% brack_2)
  return(weights)
}


bayes_stein <- function(return_matrix, gamma){
  M <- nrow(return_matrix)
  N <- ncol(return_matrix)
  
  einheits <- as.matrix(rep(1, N))
  mean_returns <- t(t(rep(1/M, M)) %*% return_matrix)
  
  
  min_weights <- as.matrix(min_var(return_matrix, gamma))
  mean_min_return <- as.numeric(t(mean_returns) %*% min_weights)
  
  ## Caclulate sigma
  sigma <- matrix(0, nrow = N, ncol = N)
  for (i in 1:M) {
    return_time_t <- as.matrix(return_matrix[i,])
    mult_sigma <- return_time_t - mean_returns
    sigma <- sigma +  mult_sigma %*% t(mult_sigma)
  }
  sigma <- (1/(M-N-2)) * sigma
  invers_simga <- solve(sigma)
  
  ## Caculate phi
  mult_phi <- mean_returns - mean_min_return
  phi <-  (N+2)/(N + 2 + M * as.numeric(t(mult_phi) %*% invers_simga %*% mult_phi))
  
  ## Mean bayes return
  mean_bayes_return <- (1-phi) * mean_returns + phi * mean_min_return
  
  ## Solve mean variance
  brack_1 <- as.numeric((t(einheits) %*% invers_simga %*% mean_bayes_return - gamma)/(t(einheits) %*% invers_simga %*% einheits ))
  brack_2 <- mean_bayes_return - brack_1 * einheits
  weights <- as.vector((1/gamma) * invers_simga %*% brack_2)
  return(weights)
}