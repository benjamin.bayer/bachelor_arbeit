### https://www.math.leidenuniv.nl/scripties/Engels.pdf
library(quadprog)

one_over_n <- function(return_matrix, gamma) { 
  number_of_assets <- dim(return_matrix)[2]
  return(rep(1/number_of_assets, number_of_assets))
}

min_var <- function(return_matrix, gamma) {
  N <- ncol(return_matrix)
  sigma <- cov(return_matrix)
  einheits <- as.matrix(rep(1, N))
  weights <- solve(sigma) %*% einheits / as.numeric(t(einheits) %*% solve(sigma) %*% einheits)
  return(as.vector(weights))
}


markowitz <-function(return_matrix, gamma) {
  M <- nrow(return_matrix)
  N <- ncol(return_matrix)
  mean_returns <- as.vector(t(rep(1/M, M)) %*% return_matrix)
  sigma <- cov(return_matrix)
  
  
  
  ##http://www.wdiam.com/b/2012/06/10/mean-variance-portfolio-optimization-with-r-and-quadratic-programming/
  Amat <- matrix(1, nrow = N)
  bvec <- 1
  meq <- 1
  dvec <- mean_returns 
  Dmat <- sigma * gamma/2
  qp <- solve.QP(Dmat = Dmat, dvec = dvec, Amat = Amat, bvec = bvec, meq=meq)
  weights <- qp$solution
  return(weights)
}

markowitz_longonly <-function(return_matrix, gamma) {
  M <- nrow(return_matrix)
  N <- ncol(return_matrix)
  mean_returns <- as.vector(t(rep(1/M, M)) %*% return_matrix)
  sigma <- cov(return_matrix)
  
  
  ##http://www.wdiam.com/b/2012/06/10/mean-variance-portfolio-optimization-with-r-and-quadratic-programming/
  Amat <-  matrix( c(rep(1,N), diag(nrow=N)), nrow=N)
  bvec <- c(1, rep(0,N))
  meq <- 1
  dvec <- mean_returns
  Dmat <- sigma * gamma/2
  qp <- solve.QP(Dmat = Dmat, dvec = dvec, Amat = Amat, bvec = bvec, meq=meq)
  weights <- qp$solution
  return(weights)
}


bayes_stein <- function(return_matrix, gamma){
  M <- nrow(return_matrix)
  N <- ncol(return_matrix)
  
  mean_returns <- t(t(rep(1/M, M)) %*% return_matrix)
  
  
  min_weights <- as.matrix(min_var(return_matrix, gamma))
  mean_min_return <- as.numeric(t(mean_returns) %*% min_weights)
  
  ## Caclulate sigma
  sigma <- matrix(0, nrow = N, ncol = N)
  for (i in 1:M) {
    return_time_t <- as.matrix(return_matrix[i,])
    mult_sigma <- return_time_t - mean_returns
    sigma <- sigma +  mult_sigma %*% t(mult_sigma)
  }
  sigma <- (1/(M-N-2)) * sigma
  invers_sigma <- solve(sigma)
  
  ## Caculate phi
  mult_phi <- mean_returns - mean_min_return
  phi <-  (N+2)/(N + 2 + M * as.numeric(t(mult_phi) %*% invers_sigma %*% mult_phi))
  
  ## Mean bayes return
  mean_bayes_return <- (1-phi) * mean_returns + phi * mean_min_return
  
  ## Solve mean variance x
  Amat = matrix(1, nrow = N)
  bvec = 1
  meq = 1
  dvec <- mean_bayes_return 
  Dmat <- sigma * gamma/2
  qp <- solve.QP(Dmat= Dmat, dvec = dvec, Amat = Amat, bvec = bvec, meq=meq)
  weights <- qp$solution
  return(weights)
}

bayes_stein_longonly <- function(return_matrix, gamma){
  M <- nrow(return_matrix)
  N <- ncol(return_matrix)
  
  mean_returns <- t(t(rep(1/M, M)) %*% return_matrix)
  
  min_weights <- as.matrix(min_var(return_matrix, gamma))
  mean_min_return <- as.numeric(t(mean_returns) %*% min_weights)
  
  ## Caclulate sigma
  sigma <- matrix(0, nrow = N, ncol = N)
  for (i in 1:M) {
    return_time_t <- as.matrix(return_matrix[i,])
    mult_sigma <- return_time_t - mean_returns
    sigma <- sigma +  mult_sigma %*% t(mult_sigma)
  }
  sigma <- (1/(M-N-2)) * sigma
  invers_sigma <- solve(sigma)
  
  ## Caculate phi
  mult_phi <- mean_returns - mean_min_return
  phi <-  (N+2)/(N + 2 + M * as.numeric(t(mult_phi) %*% invers_sigma %*% mult_phi))
  
  ## Mean bayes return
  mean_bayes_return <- (1-phi) * mean_returns + phi * mean_min_return
  
  ## Solve mean variance x
  Amat <-  matrix( c(rep(1,N), diag(nrow=N)), nrow=N)
  bvec <- c(1, rep(0,N))
  meq <- 1
  dvec <- mean_bayes_return 
  Dmat <- sigma * gamma/2
  qp <- solve.QP(Dmat= Dmat, dvec = dvec, Amat = Amat, bvec = bvec, meq=meq)
  weights <- qp$solution
  return(weights)
}



