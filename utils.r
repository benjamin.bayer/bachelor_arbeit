#######
tibble_to_matrix_for_rolling_window <- function(return_data, window) {
  return(as.matrix(return_data[window,]))
}

#######
weights_for_period_n <- function(return_data, strategy, window, gamma){
  window_data_for_period_n <- tibble_to_matrix_for_rolling_window(return_data, window)
  return(strategy(window_data_for_period_n, gamma))
}

#######
weights_for_all_periods <- function(return_data, strategy, window_length, gamma = 1){
  n_predicted_periods <- nrow(return_data) - window_length
  n_assets <- ncol(return_data)
  assets_name <- colnames(return_data)
  weights <- matrix(nrow = n_predicted_periods, ncol = n_assets)
  colnames(weights) <- assets_name
  for (i in 1:n_predicted_periods) {
    window <- i:(window_length-1+i)
    weights[i,] <- weights_for_period_n(return_data, strategy, window, gamma)
  }
  weights <- xts(weights,  index(return_data)[(window_length + 1) : nrow(return_data)])
  return(weights)
}

#######
portfolio_return_timeseries <- function(weights, return_data, transaction_costs_flag = T, transaction_cost = 0.0005) {
  M_weights <- nrow(weights)
  M_return <- nrow(return_data)
  relevant_return_start <- M_return - M_weights + 1
  relevant_return_end <- M_return
  relevant_returns <- tibble_to_matrix_for_rolling_window(return_data)[relevant_return_start:relevant_return_end,]
  weighted_returns <- weights * relevant_returns
  entries <- relevant_return_end - relevant_return_start + 1
  portfolio_return_without_transaction_cost <- rep(0, entries)
  weights_t_in_tplus1 <- stats::lag(weights * (relevant_returns + 1))
    
  # considering transaction costs 
  weights_delta_tplus1_t <- abs(weights - weights_t_in_tplus1)
  portfolio_return_with_transaction_cost <- rep(0, entries)
  
  
  for (i in 1:entries) {
    portfolio_return_without_transaction_cost[i] <- sum(weighted_returns[i,])
    if (i == 1 && transaction_costs_flag == T){
      portfolio_return_with_transaction_cost[i] <- (1 + portfolio_return_without_transaction_cost[i]) * (1 - transaction_cost) - 1
    }
    if (i != 1 && transaction_costs_flag == T){
      portfolio_return_with_transaction_cost[i] <- (1 + portfolio_return_without_transaction_cost[i]) * (1 - transaction_cost * sum(weights_delta_tplus1_t[i,])) - 1 
    }
  }
  if (transaction_costs_flag == T){
    return(portfolio_return_with_transaction_cost)
  } else if (transaction_costs_flag == F) {
    return(portfolio_return_without_transaction_cost)
  }
}


weighted_returns <- excess_returns[51:nrow(excess_returns)] * markowitz_weights_1
port_returns <- rep(0, nrow(weighted_returns))


for (i in 1:nrow(weighted_returns)) {
  port_returns[i] <- sum(weighted_returns[i,])
}

